using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{

    public static UiManager instance;

    [SerializeField] Image fader;

    private void Start()
    {
        instance = this;
    }

    public void FadeOut()
    {
        fader.GetComponent<Animator>().SetTrigger("NextLevel");
    }

}
