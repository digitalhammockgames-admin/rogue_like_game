using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakables : MonoBehaviour
{

    [SerializeField] GameObject[] brokenParts;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.CompareTag("Player")){

            bool playerIsDashing = collision.gameObject.GetComponent<PlayerController>().PlayerIsDashing();

            if (playerIsDashing)
            {
                GetComponent<Animator>().SetTrigger("break");
                for (int i = 0; i < brokenParts.Length; i++)
                {
                    int randomPart = Random.Range(0, brokenParts.Length);
                    int randomRotation = Random.Range(0, 4);

                    Instantiate(brokenParts[randomPart], 
                        transform.position, 
                        Quaternion.Euler(0f,0f,90f * randomRotation));
                }
            }
        }
    }

    public void Destroy()
    {
        
        Destroy(gameObject);
    }
}
