using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] int enemyHealth = 100;
    [SerializeField] float enemySpeed;
    private Rigidbody2D enemyRigidbody;

    [SerializeField] float playerChaseRange;
    [SerializeField] float keepChasing;
    [SerializeField] GameObject bloodSplatter;
    private Vector3 directionToMoveIn;
    private Transform playerToChase;
    private bool isChasing = false;
    private Animator enemyAnim;


    // attack stuff
    [SerializeField] bool meleeAttacker;

    [SerializeField] GameObject enemyProjectile;
    [SerializeField] Transform firePosition;


    [SerializeField] float timeBetweenShots;
    [SerializeField] float shootingRange;
    private bool readyToShoot;

    // Start is called before the first frame update
    void Start()
    {
        enemyRigidbody = GetComponent<Rigidbody2D>();
        playerToChase = FindObjectOfType<PlayerController>().transform;
        enemyAnim = GetComponentInChildren<Animator>();
        readyToShoot = true;
        }

    // Update is called once per frame
    void Update()
    {
        EnemyMovement();
        EnemyShooting();
    }

    private void EnemyShooting()
    {
        if (!meleeAttacker &&
                    readyToShoot &&
                    Vector3.Distance(transform.position, playerToChase.position) < shootingRange)
        {
            readyToShoot = false;
            // shoot the bullet
            StartCoroutine(FireEnemyProjectile());

        }
    }

    private void EnemyMovement()
    {
        if (Vector3.Distance(transform.position, playerToChase.position) < playerChaseRange)
        {
            directionToMoveIn = playerToChase.position - transform.position;
            isChasing = true;

        }
        else if ((Vector3.Distance(transform.position, playerToChase.position) < keepChasing) && isChasing)
        {
            directionToMoveIn = playerToChase.position - transform.position;
        }
        else
        {
            directionToMoveIn = Vector3.zero;
            isChasing = false;
        }

        directionToMoveIn.Normalize();
        enemyRigidbody.velocity = directionToMoveIn * enemySpeed;

        if (directionToMoveIn != Vector3.zero)
        {
            enemyAnim.SetBool("isWalking", true);
        }
        else
        {
            enemyAnim.SetBool("isWalking", false);
        }

        if (playerToChase.position.x < transform.position.x)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        else
        {
            transform.localScale = Vector3.one;
        }
    }

    IEnumerator FireEnemyProjectile()
    {
        

        Instantiate(enemyProjectile, firePosition.position, firePosition.rotation);
        yield return new WaitForSeconds(timeBetweenShots);
        readyToShoot = true;
    }

    public void DamageEnemy(int damageTaken) {
        enemyHealth -= damageTaken;

        if(enemyHealth <= 0)
        {
            Instantiate(bloodSplatter, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, playerChaseRange);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, keepChasing);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, shootingRange);
    }

}
