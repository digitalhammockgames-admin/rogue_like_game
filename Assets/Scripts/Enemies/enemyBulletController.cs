using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBulletController : MonoBehaviour
{

    [SerializeField] float bulletSpeed;
    private Vector3 playerDirection;
    private Rigidbody2D bulletRigidBody;

    private Animator bulletAnim;
    // Start is called before the first frame update
    void Start()
    {
        playerDirection = FindObjectOfType<PlayerController>().transform.position - transform.position;
        playerDirection.Normalize();

        bulletAnim = GetComponentInChildren<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        transform.position += playerDirection * bulletSpeed * Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player")){
            
            Debug.Log("Player was hit!");
        }

        Destroy(gameObject);
    }
}
