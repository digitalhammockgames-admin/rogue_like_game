using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] float bulletSpeed = 5f;
    [SerializeField] GameObject bulletImpactEffect;
    [SerializeField] GameObject[] damageEffects;
    [SerializeField] int damageAmount = 10;

    private Rigidbody2D bulletRigidBody;


    void Start()
    {
        bulletRigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        bulletRigidBody.velocity = transform.right * bulletSpeed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        

        if (collision.CompareTag("Enemy"))
        {
            int selectedSplatter = Random.Range(0, damageEffects.Length);
            
            Instantiate(damageEffects[selectedSplatter].transform, transform.position, transform.rotation);
            

            collision.GetComponent<EnemyController>().DamageEnemy(damageAmount);
        }
        else
            Instantiate(bulletImpactEffect.transform, transform.position, transform.rotation);


        Destroy(gameObject);
    }
}
