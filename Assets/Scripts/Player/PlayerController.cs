using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] int movementSpeed = 3;

    [SerializeField] Rigidbody2D playerRigidbody;

    private Camera mainCamera;

    [SerializeField] Transform weaponsArm;

    private Vector2 movementInput;

    private Animator playerAnimator;


    // dash mechanics

    private float currentMoveMentSpeed;
    private bool canDash;
    public bool isDashing = false;
    [SerializeField] float dashSpeed = 6f, dashLength = 0.5f, dashCooldown = 2f;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;

        playerAnimator = GetComponent<Animator>();

        currentMoveMentSpeed = movementSpeed;

        canDash = true;
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMovement();
        PlayerAim();
        //PlayerShooting();

        if(Input.GetKeyDown(KeyCode.Space) && canDash)
        {
            PlayerDash();
        }

    }

    public bool PlayerIsDashing()
    {
        if (currentMoveMentSpeed == dashSpeed)
            return true;
        else
            return false;
    }

    private void PlayerDash()
    {
        currentMoveMentSpeed = dashSpeed;
        canDash = false;
        playerAnimator.SetTrigger("Dash");
        // Access the Player Health Handler and make sure he's invincible during dash.


        // dash boot for dasg length.
        StartCoroutine(DashBoost());
        StartCoroutine(DashCoolingDown());
        
    }

    IEnumerator DashBoost()
    {
        yield return new WaitForSecondsRealtime(dashLength);
        currentMoveMentSpeed = movementSpeed;
    }

    IEnumerator DashCoolingDown()
    {
        yield return new WaitForSecondsRealtime(dashCooldown);
        canDash = true;
    }

    

    private void PlayerMovement()
    {
        movementInput.x = Input.GetAxisRaw("Horizontal");
        movementInput.y = Input.GetAxisRaw("Vertical");

        movementInput.Normalize();

        playerRigidbody.velocity = currentMoveMentSpeed * movementInput;

        if(movementInput != Vector2.zero)
        {
            playerAnimator.SetBool("isWalking", true);
        } else
            playerAnimator.SetBool("isWalking", false);


    }

    private void PlayerAim()
    {
        Vector3 mousePosition = Input.mousePosition;
        Vector3 screenPoint = mainCamera.WorldToScreenPoint(transform.localPosition);

        Vector2 offSet = new Vector2(mousePosition.x - screenPoint.x, mousePosition.y - screenPoint.y);

        float angle = Mathf.Atan2(offSet.y, offSet.x) * Mathf.Rad2Deg;

        weaponsArm.rotation = Quaternion.Euler(0, 0, angle);

        if(mousePosition.x < screenPoint.x)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
            weaponsArm.localScale = new Vector3(-1f, -1f, 1f);
        }
        else
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
            weaponsArm.localScale = new Vector3(1f, 1f, 1f);
        }
    }
}
