using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsSystem : MonoBehaviour
{

    [SerializeField] GameObject bullet;
    [SerializeField] Transform firePosition;
    [SerializeField] float timeBetweenShots = 1f;
    private float shotCounter = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FiringBullets();
    }


    private void FiringBullets()
    {

        if (GetComponentInParent<PlayerController>().PlayerIsDashing())
        {
            return;
        }

        if (shotCounter > 0)
        {
            shotCounter -= Time.deltaTime;
        }
        else
        {
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0)){
                Instantiate(bullet, firePosition.position, firePosition.rotation);
                shotCounter = timeBetweenShots;
            }
        }

        //if (Input.GetMouseButtonDown(0) && !isWeaponAuto)
        //{
        //    Instantiate(bullet, firePosition.position, firePosition.rotation);
        //}
        //if (Input.GetMouseButton(0) && isWeaponAuto)
        //{
        //    shotCounter -= Time.deltaTime;

        //    if (shotCounter <= 0)
        //    {
        //        Instantiate(bullet, firePosition.position, firePosition.rotation);
        //        shotCounter = timeBetweenShots;
        //    }

        //}
    }
}
